#pragma once

#include <stdint.h>

typedef void(*bootvector_t)();
bootvector_t load_ios(const char* path);

//don't overwrite .init
#define LOADER_PROTECTED_ZONE 0xFFFF0000
#define LOADER_PROTECTED_ZONE_SZ 0x00010000

void loader_finish_pzone();
void* loader_get_pzone();
