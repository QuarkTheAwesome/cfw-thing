#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "ios-loader.h"
#include "ancast.h"
#include "elf.h"
#include "utils.h"
#include "memory.h"

#define ANCAST_BASEADDR 0x01000000

/* Zones to avoid messing with */
static uint32_t notouch_zones[] = { /* No zones right now */ };
static uint32_t notouch_zones_count =
    sizeof(notouch_zones) / sizeof(notouch_zones[0]);

static void* loader_pzone_storage = 0;

bootvector_t load_ios(const char* path) {
/*  Load and decrypt the ios image to ANCAST_BASEADDR */
    uint32_t bootvector = ancast_iop_load(path);
    if (!bootvector) {
        printf("ios-loader: Couldn't load ancast!\n");
        return 0;
    }

/*  A standard IOSU ancast image has:
    - The ancast header (first 0x300ish bytes)
    - A bloody tiny elf loader (0x500ish bytes)
    - The IOSU elf itself (0x804 and onwards for 5.5.x)
    The elf offset depends on the size of the elfloader, so we go search for
    something that looks like an ELF rather than hardcoding the offset. */
    uint32_t elf = 0;
    for (uint32_t ptr = ANCAST_BASEADDR; ptr < ANCAST_BASEADDR + 0xFFF; ptr++) {
    /*  All elf files have this magic at the start, mighty convenient
        If I were being pedantic I'd check the other 12 bytes of the magic */
        if (memcmp("\x7F" "ELF", (void*)ptr, 4) == 0) {
            printf("ios-loader: Found elf at 0x%08lX!\n", ptr);
            elf = ptr;
            break;
        }
    }
    if (!elf) {
        printf("ios-loader: Couldn't find elf in ancast image.\n");
        return 0;
    }

    Elf32_Ehdr* ehdr = (Elf32_Ehdr*)elf;

/*  Make sure a valid program header offset and number of program headers is
    given */
    if (!ehdr->e_phoff || !ehdr->e_phnum) {
        printf("ios-loader: No program headers! Cannot load elf.\n");
        return 0;
    }

/*  Get a pointer to the array of program headers */
    Elf32_Phdr* phdrs = (Elf32_Phdr*)(elf + ehdr->e_phoff);
    uint32_t count = ehdr->e_phnum;
    for (int i = 0; i < count; i++) {
        Elf32_Phdr phdr = phdrs[i];

        if (phdr.p_type != PT_LOAD) {
            printf("ios-loader: Skipping program header %d, type %08lX\n",
                i, phdr.p_type);
            continue;
        }
    /*  We've got a PT_LOAD, which means we need to copy some data from the elf
        to some place in memory. */

    /*  Calculate the source/destination addresses and the size of the copy */
        uint32_t load_src = elf + phdr.p_offset;
        uint32_t load_dst = phdr.p_paddr;
        uint32_t load_sz = phdr.p_filesz;
    /*  If p_memsz > p_filesz, it's some kind of BSS and we should zero it.
        In practice, memsz = filesz (normal section) or filesz = 0 (bss) */
        uint32_t zero_dst = phdr.p_paddr + phdr.p_filesz;
        uint32_t zero_sz = (phdr.p_memsz > phdr.p_filesz) ?
            phdr.p_memsz - phdr.p_filesz : 0;

    /*  Check if this area is listed in notouch_zones, and if so, bail */
        bool zone_safe = true;
        for (int j = 0; j < notouch_zones_count; j++) {
            if (notouch_zones[j] == load_dst) {
                printf("ios-loader: Skipping notouch zone %08lX\n", load_dst);
                zone_safe = false;
                break;
            } else if (notouch_zones[j] == zero_dst) {
                printf("ios-loader: Won't zero notouch zone %08lX\n", zero_dst);
                zero_sz = 0;
            }
        }
        if (!zone_safe) continue;

        if (load_dst == LOADER_PROTECTED_ZONE) {
            printf("ios-loader: not overwriting protected zone %08X!\n",
                LOADER_PROTECTED_ZONE);

            loader_pzone_storage = memalign(0x10, LOADER_PROTECTED_ZONE_SZ);
            memset(loader_pzone_storage, 0, LOADER_PROTECTED_ZONE_SZ);
            printf("ios-loader: loading into temp area %08lX\n",
                (uint32_t)loader_pzone_storage);

            load_dst = (uint32_t)loader_pzone_storage;
        }

    /*  Copy! */
        memcpy((void*)load_dst, (void*)load_src, load_sz);
    /*  Zero! */
        if (zero_sz) {
            memset((void*)zero_dst, 0, zero_sz - 1);
        }
    }

/*  Return the entrypoint given in the ELF header. For most images, this is
    0xFFFF0000 - the start of the IOSU kernel. */
    return (bootvector_t)ehdr->e_entry;
}

void* loader_get_pzone() {
    return loader_pzone_storage;
}

void loader_finish_pzone() {
    if (loader_pzone_storage) {
        printf("ios-loader: moving temporary area at %08lX to pzone %08X\n",
            (uint32_t)loader_pzone_storage, LOADER_PROTECTED_ZONE);

        memcpy(
            (void*)LOADER_PROTECTED_ZONE,
            loader_pzone_storage,
            LOADER_PROTECTED_ZONE_SZ
        );
        free(loader_pzone_storage);
        loader_pzone_storage = NULL;
    } else {
        printf("ios-loader: no pzone, is this really a Cafe image?\n");
    }
}
