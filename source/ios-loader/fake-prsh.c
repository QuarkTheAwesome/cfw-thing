/*
    CFW Thing alpha
    (C) 2018 Ash Logan <ash@heyquark.com>
    PRSH stuff credit to Hexkyz <https://twitter.com/hexkyz> whose blog post
    proved invaluable. I copied all your values, hope that's ok 😅
*/

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include "minute/utils.h"

#define PRSH_ARENA 0x10000000
#define PRSH_ARENA_SIZE 0x00100000
// magic numbers, freshly dumped from 5.5.3
#define PRSH_LOCATION (PRSH_ARENA + 0x5A54)
#define PRSH_SIZE 0x259C
#define PRST_LOCATION (PRSH_LOCATION + PRSH_SIZE)

#define BOOTINFO_LOCATION 0x10008000
#define BOOTINFO_SIZE 0x58

typedef struct {
  char name[0x100];
  void* data;
  uint32_t size;
  uint32_t unk;
  uint8_t hash[0x14];
  uint8_t padding[0x0C];
} prsh_section_t;

typedef struct {
  uint32_t xor_checksum;
  uint32_t magic;
  uint32_t version;
  uint32_t size;
  uint32_t unk;
  uint32_t max_sections;
  uint32_t num_sections;
  prsh_section_t sections[];
} prsh_t;

typedef struct {
  uint32_t xor_checksum;
  uint32_t size;
  uint32_t unk;
  uint32_t magic;
} prst_t;

typedef struct {
    uint32_t unk0x0;
    uint32_t boot_flags; //0x4
    uint32_t boot_state; //0x8
    uint32_t boot_count; //0xC
    uint32_t unk0x10[10]; //0x10-0x34
    uint32_t boot1_main; //0x38
    uint32_t boot1_read; //0x3c
    uint32_t boot1_verify; //0x40
    uint32_t boot1_decrypt; //0x44
    uint32_t boot0_main; //0x48
    uint32_t boot0_read; //0x4c
    uint32_t boot0_verify; //0x50
    uint32_t boot0_decrypt; //0x54
} bootinfo_t;

void prsh_dump() {
    printf("dumping PRSH_ARENA\n");
    FILE* dump = fopen("sdmc:/prsh.dump", "wb");
    fwrite((void*)PRSH_ARENA, PRSH_ARENA_SIZE, 1, dump);
    fclose(dump);
    printf("dumped PRSH_ARENA\n");
}

void fake_prsh_create() {
/*  Try and word this log message as "breakthrough"-y as possible */
    printf("fake-prsh: pretending to be boot1 to simulate coldboot\n");

/*  A generous memset */
    memset((void*)PRSH_ARENA, 0, 0x10000);
    prsh_t* prsh = (prsh_t*)PRSH_LOCATION;
    prst_t* prst = (prst_t*)PRST_LOCATION;
    bootinfo_t* bootinfo = (bootinfo_t*)BOOTINFO_LOCATION;

/*  Initialise all the structs */
    *prsh = (prsh_t) {
        .magic = 0x50525348, //"PRSH"
        .version = 1,
        .size = PRSH_SIZE,
        .unk = 1, //all the dumps have this set to 1
        .max_sections = 0x20,
        .num_sections = 1,
    };
    prsh->sections[0] = (prsh_section_t) {
        .name = "boot_info",
        .data = (void*)BOOTINFO_LOCATION,
        .size = BOOTINFO_SIZE,
        .unk = 0x80000000,
    };

/*  calc PRSH checksum */
    uint32_t checksum = 0;
/*  start at 1 to skip XOR stuff, 0x2000 is close enough to sizeof(prsh) */
    for (int i = 1; i < 0x2000/4; i++) {
        checksum ^= ((uint32_t*)PRSH_LOCATION)[i];
    }
    prsh->xor_checksum = checksum;

    *prst = (prst_t) {
        .size = PRSH_SIZE,
        .unk = 1,
        .magic = 0x50525354, //"PRST"
        .xor_checksum = PRSH_SIZE ^ 1 ^ 0x50525354,
    };
    *bootinfo = (bootinfo_t) {
        .unk0x0 = 1,
    /*  Attempted values:
        flags = 0xA6000000 / state = 0: UVD fails, IOS thinks coldboot
            (sysmode = 0x100000, flags = 0x10000, sec = 0x1e ?? RETEST)
        flags = 0xE6000000 / state = 0; UVD fails, IOS thinks warmboot
            (sysmode = 0x100000, flags = 0x0, sec = 0x1e)
        flags = 0x06000000 / state = 0; IOS doesn't boot
        flags = 0xA0000000 / state = 0; UVD fails, coldboot
            (sysmode = 0x100000, flags = 0x10000, sec = 0x1e)
        flags = 0xF0000000 / state = 0; UVD fails, warmboot
            (sysmode = 0x100000, flags = 0x0, sec = 0x1e)
        flags = 0x80000000 / state = 0; IOS doesn't boot
        flags = 0x20000000 / state = 0; IOS doesn't boot
        flags = 0xAF000000 / state = 0; UVD fails, coldboot */
        .boot_flags = 0xAF000000, //also seen 0xE6
        .boot_state = 0,
        .boot_count = 1,
        .unk0x10 = {
            1, //could also be 0x00100000
            0,
            -1,
            -1,
            -1,
            -1,
            -1,
            -1,
            0,
            0,
        },
    /*  Timings taken from my console */
        .boot1_main = 0x36D05F,
        .boot1_read = 0x29A359,
        .boot1_verify = 0x5FCFE,
        .boot1_decrypt = 0x53CE7,
        .boot0_main = 0x12052,
        .boot0_read = 0x29F4,
        .boot0_verify = 0xD281,
        .boot0_decrypt = 0x27A,
    };
    printf("fake-prsh: booting w/ %08lX %08lX", bootinfo->boot_flags, bootinfo->boot_state);
}
