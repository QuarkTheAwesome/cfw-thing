#pragma once

#include <stdbool.h>

#define PATCHER_PATCH_EXT "elf"

void patcher_run();

bool patcher_apply_directory(const char* path);
bool patcher_apply_file(const char* path);
