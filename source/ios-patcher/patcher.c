#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <dirent.h>

#include "patcher.h"

void patcher_run() {
    patcher_apply_directory("sdmc:/wiiu/cfw/sys");
}

bool patcher_apply_directory(const char* path) {
    DIR* dir;
    struct dirent* ent;
    bool result;

    //BUG: this never returns
    dir = opendir(path);
    if (!dir) {
        printf("ios-patcher: Couldn't open %s!\n", path);
        return false;
    }

    while ((ent = readdir(dir))) {
        if (ent->d_name[0] == '\0' || ent->d_name[0] == '.') {
            printf("ios-patcher: skipping %s\n", ent->d_name);
            continue;
        }
        if (ent->d_type != DT_REG) {
            printf("ios-patcher: %s isn't a file!\n", ent->d_name);
            continue;
        }

        char filepath[strlen(path) + strlen(ent->d_name) + 2];
        sprintf(filepath, "%s/%s", path, ent->d_name);

        bool patchresult = patcher_apply_file(filepath);
        if (!patchresult) result = false;
    }

    return result;
}

bool patcher_apply_file(const char* path) {
    printf("ios-patcher: Going to apply %s\n", path);
    return true;
}
