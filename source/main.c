/*
    CFW Thing alpha
    (C) 2018 Ash Logan <ash@heyquark.com>
*/

#include <stdint.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include "main.h"
#include "latte.h"
#include "mlc.h"
#include "memory.h"
#include "gfx.h"
#include "irq.h"
#include "exception.h"
#include "crypto.h"
#include "nand.h"
#include "sdcard.h"
#include "fatfs/fs_dev.h"
#include "fatfs/sdcard_partition.h"
#include "isfs.h"
#include "smc.h"
#include "ancast.h"
#include "utils.h"
#include "ios-patcher/patcher.h"
#include "ios-loader/ios-loader.h"
#include "ios-loader/fake-prsh.h"

noreturn void _main(void* base) {
    (void)base;
    int rc;

/*  Set up logging to the screen */
    gfx_clear(GFX_ALL, BLACK);
    gfx_init_log();
    printf("CFW Thing alpha\n");

/*  Hardware guff, woohoo! Exception handler, MMU, caches, interrupts etc. */
    exception_initialize();
    mem_initialize();
    irq_initialize();

/*  do we *really* need strong crypto here */
    srand(read32(LT_TIMER));
    crypto_initialize();

    printf("Mounting filesystems...\n");
    sdcard_init();
    rc = sdcard_mount_fs();
    if (rc) {
        printf("Failed to mount SD card.\n");
    }

/*  Init the SLC */
    isfs_init();

/*  Try to load the Cafe default IOSU image from SLC */
    printf("Loading IOSU image...\n");
    bootvector_t bootvector = load_ios("slc:/sys/title/00050010/1000400a/code/fw.img");
    if (!bootvector) {
        printf("Error while loading IOSU!\n");
        panic(0);
    }

/*  Tested Cafe IOSU images wipe the boot1 PRSH/PRST, so let's fake one
    Bonus: this makes IOSU think it's a coldboot, so this is boot1hax-ready! */
    fake_prsh_create();
    prsh_dump();

/*  Run patcher to, well, apply patches. */
    patcher_run();

    printf("DEBUG: patching for syshax.xml\n");
    memcpy((void*)0x082200DC, "/vol/system/config/syshax.xml", 0x20);
    memcpy((void*)0x082200FC, "/vol/system_slc/config/syshax.xml", 0x24);

/*  Yeah, that's legit it. Let's turn all that hardware back off again */
    printf("Cleaning up...\n");
    isfs_fini();
    fsdev_unmount_all();
    sdcard_exit();
    irq_shutdown();
    mem_shutdown();

/*  The original IOSU loader does all these things. Not sure why, but copying
    it is probably a good idea. */
    set32(LT_MEMIRR, 0x20);
    write32(OHCI0_REG_BASE + 4, 0);
    write32(OHCI1_REG_BASE + 4, 0);
    write32(OHCI10_REG_BASE + 4, 0);
    write32(OHCI20_REG_BASE + 4, 0);

/*  The loader might have avoided loading something to keep us running normally.
    We're now good for whatever it was to be overwritten. */
    loader_finish_pzone();

    printf("Booting Cafe... (vec=%08lX)\n", (uint32_t)bootvector);
    gfx_deinit_log();

/*  This function pointer *should* be the entrypoint for the loaded image */
    bootvector();
    for (;;) {}
}
