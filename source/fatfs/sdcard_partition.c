#include <string.h>

#include "minute/sdcard.h"
#include "minute/sdhc.h"
#include "fatfs/fs_dev.h"
#include "minute/utils.h"

static int sdcard_fs_initialize(device_partition_t* devpart) {
    devpart->crypto_work_buffer = NULL;
    devpart->crypto_work_buffer_num_sectors = 0;

    if (sdcard_check_card() == SDMMC_NO_CARD) {
        return -1;
    }
    sdcard_ack_card();

    devpart->num_sectors = sdcard_get_sectors();

    //todo: if (sdcard_init()) ...
    devpart->initialized = true;
    return 0;
}

static void sdcard_fs_finalize(device_partition_t* devpart) {
    //currently sdcard system init/teardown is handled by app
}

static u8 sdcard_fs_workbuf[SDMMC_DEFAULT_BLOCKLEN * SDHC_BLOCK_COUNT_MAX] ALIGNED(32);

static int sdcard_fs_read(device_partition_t* devpart, void* dst, uint64_t sector, uint64_t num_sectors) {
    int rc;

    while (num_sectors > 0) {
        uint64_t work = min(num_sectors, SDHC_BLOCK_COUNT_MAX);

        rc = sdcard_read(sector, work, sdcard_fs_workbuf);
        if (rc) return EIO;

        memcpy(dst, sdcard_fs_workbuf, work * SDMMC_DEFAULT_BLOCKLEN);

        sector += work;
        num_sectors -= work;
        dst += work * SDMMC_DEFAULT_BLOCKLEN;
    }

    return 0;
}

static int sdcard_fs_write(device_partition_t* devpart, const void* src, uint64_t sector, uint64_t num_sectors) {
#if defined(SDCARD_WRITE)
    int rc;

    while (num_sectors > 0) {
        uint64_t work = min(num_sectors, SDHC_BLOCK_COUNT_MAX);

        memcpy(sdcard_fs_workbuf, src, work * SDMMC_DEFAULT_BLOCKLEN);

        rc = sdcard_write(sector, work, sdcard_fs_workbuf);
        if (rc) return EIO;

        sector += work;
        num_sectors -= work;
        src += work * SDMMC_DEFAULT_BLOCKLEN;
    }

    return 0;
#else
    return EIO;
#endif
}

static const device_partition_t sdcard_fs = {
    .sector_size = SDMMC_DEFAULT_BLOCKLEN,
    .initializer = sdcard_fs_initialize,
    .finalizer = sdcard_fs_finalize,
    .reader = sdcard_fs_read,
    .writer = sdcard_fs_write,
};

int sdcard_mount_fs() {
    int rc;

    rc = fsdev_mount_device("sdmc", &sdcard_fs, true);
    if (rc == -1) return rc;

    rc = fsdev_register_device("sdmc");
    if (rc == -1) return rc;

    return 0;
}
